# coding: utf-8
require "twitter"
require "nokogiri"
require "open-uri"
require "cgi"

JAPAN = 23424856
CONSUMER_KEY        = ENV['CONSUMER_KEY']
CONSUMER_SECRET     = ENV['CONSUMER_SECRET']
ACCESS_TOKEN        = ENV['ACCESS_TOKEN']
ACCESS_TOKEN_SECRET = ENV['ACCESS_TOKEN_SECRET']
OUTPUT_DIR = './generated_articles'
PARTIAL_LEN = 50

def get_japanese_trend_words_from_twitter
  client = Twitter::REST::Client.new do |config|
    config.consumer_key        = CONSUMER_KEY
    config.consumer_secret     = CONSUMER_SECRET
    config.access_token        = ACCESS_TOKEN
    config.access_token_secret = ACCESS_TOKEN_SECRET
  end

  return client.local_trends(JAPAN).map { |trend| trend.name }
end

def get_articles_from_google(keyword)
  contents = ""
  keyword = keyword.gsub('#', '')
  p keyword
  escaped_keyword = CGI.escape(keyword)
  url = ""

  begin
    doc = Nokogiri.HTML(open("http://www.google.co.jp/search?ie=UTF-8&oe=UTF-8&q=#{escaped_keyword}"))
    doc.search("div#search ol h3 a").each_with_index do |a, idx|
      next if idx == 0

      title = a.inner_html.gsub(/<b>/, "").gsub(/<\/b>/, "")
      url =  a.attribute("href").value.scan(/q=http.+\&sa/).join.gsub('q=', '').gsub('&sa', '')

      begin
        page = Nokogiri.HTML(open(url))
      rescue Exception => e
        p e
        p url
        next
      end

      partials = page.to_s.scrub.scan(/[^\x00-\x7F]{#{PARTIAL_LEN},}/)

      if partials[0] != nil
        contents += "<h3>#{title} より</h3>"
        # puts "url   : #{url}"
        contents += "<p>#{partials[0].encode('UTF-8')}</p>"
      end
    end
  rescue Exception => e
    p e
    return "miss"
  end

  return contents
end

def generate_html_page(contents, keyword)
  html =  '
<!DOCTYPE html>
<html lang=”ja”>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>' + keyword +  '</title>
</head>
<h1>' + keyword + '</title>
<body>' + contents + '</body>
</html>'

  File.write("#{OUTPUT_DIR}/#{keyword}.html", html)
end

FileUtils.mkdir_p(OUTPUT_DIR) unless FileTest.exist?(OUTPUT_DIR)

trends = get_japanese_trend_words_from_twitter

trends.each_with_index do |keyword, idx|
  contents = get_articles_from_google(keyword)
  generate_html_page(contents, keyword)
end
